--[[
    ARCade
    Copyright (C) 2018  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

--]]
--snake game in lua


focus = true
Fullscreen = false
accumulator = 0

window = {}
    function window:update()
        window.width, window.height, f = love.window.getMode()
        window.h = window.height / 34
        window.w = window.width / 34
        window.hborder = window.h / 10
        window.wborder = window.w / 10
    end

fruit = {}
    function fruit:update()
        repeat
            valid = true 
            fruit.x = math.random(2, 32)
            fruit.y = math.random(2, 32)
            for i = 1, #snake.body, 1 do
                if snake.body[i][1] == fruit.x and snake.body[i][2] == fruit.y then
                    valid = false 
                end 
            end
        until valid
    end

snake = {
    body = {},
    moveSpeed = 1,
    moveDir = 'right',
    lastMove = 'right',
    old = {16, 17}
}

game = {
    pause = false,
    fail = false,
    score = 0
}

    function game:grow()
        fruit:update()
        game.score = game.score + 1
        table.insert(snake.body, 1, snake.old)
    end

    function game:restart()
        snake.body = {}
        snake.moveDir = 'right'
        snake.lastMove = 'right'
        game.pause = false
        game.fail = false
        game.score = 0
        for i = 1,3,1 do
            snake.body[i] = {16 + i, 17}
        end

        fruit:update()
    end

function love.load()
    snake.body = {}
    snake.moveDir = 'right'
    snake.lastMove = 'right'
    game.pause = false
    game.fail = false
    game.score = 0
    for i = 1,3,1 do
        snake.body[i] = {16 + i, 17}
    end

    fruit:update()

    love.window.setTitle("Snake")
    love.window.setMode(800, 600, {resizable = true, fullscreen = Fullscreen, minwidth = 800, minheight = 600})
    love.graphics.setBackgroundColor(0, 0, 0)
    love.graphics.newFont(48)
end

function love.draw()
    love.graphics.setColor(0.25, 0.25, 0.25)
    love.graphics.rectangle('fill', window.w, window.h, window.w * 32, window.h * 32)
    love.graphics.setColor(1, 1, 1)
    love.graphics.print(string.format("SCORE : %d", game.score), 0, window.hborder)    

    if game.pause and not game.fail then
        love.graphics.setColor(1, 1, 1)
        love.graphics.print("Paused, press e to exit, esc to resume or r to restart", window.w * 16, window.h * 33) 
    end

    for i = 1, #snake.body, 1 do
        love.graphics.setColor(0, 0.5, 0.5)
        love.graphics.rectangle("fill", snake.body[i][1] * window.w + window.wborder, snake.body[i][2] * window.h + window.hborder, window.w - 2 * window.wborder, window.h - 2 * window.hborder)
    end
    
    love.graphics.setColor(0, 0.75, 0)
    love.graphics.rectangle("fill", fruit.x * window.w + window.wborder, fruit.y * window.h + window.hborder, window.w - 2 * window.wborder, window.h - 2 * window.hborder)

    if game.fail then
       love.graphics.setColor(1, 1, 1)
       love.graphics.print(string.format("You lost! SCORE: %d, press r to restart or e to exit", game.score), window.w * 16, window.h * 33) 
    end
end

function love.update(dt)
    window.update()
    if game.pause == true then
    else
        tick = 1/10
        accumulator = accumulator + dt 
        if accumulator >= tick then
            accumulator = 0
               snake.old = snake.body[1]
               table.remove(snake.body, 1)
               if snake.moveDir == 'up' then
                  table.insert(snake.body, #snake.body + 1, {snake.body[#snake.body][1], snake.body[#snake.body][2] - 1}) 
               elseif snake.moveDir == 'down' then
                  table.insert(snake.body, #snake.body + 1, {snake.body[#snake.body][1], snake.body[#snake.body][2] + 1}) 
               elseif snake.moveDir == 'left' then
                  table.insert(snake.body, #snake.body + 1, {snake.body[#snake.body][1] - 1, snake.body[#snake.body][2]}) 
               elseif snake.moveDir == 'right' then
                  table.insert(snake.body, #snake.body + 1, {snake.body[#snake.body][1] + 1, snake.body[#snake.body][2]}) 
               end
        end
        if snake.body[#snake.body][1] == fruit.x and snake.body[#snake.body][2] == fruit.y then
             game:grow()
        end
        if snake.body[#snake.body][1] == 0 or snake.body[#snake.body][1] == 33 or snake.body[#snake.body][2] == 0 or snake.body[#snake.body][2] == 33 then
            table.remove(snake.body, #snake.body)
            game.fail = true 
            game.pause = true
        end
        for i = 1, #snake.body - 1, 1 do
           if snake.body[i][1] == snake.body[#snake.body][1] and snake.body[i][2] == snake.body[#snake.body][2] then
               game.fail = true
               game.pause = true
           end 
        end
    end
end

function love.keypressed(key)
    if key == 'escape' and not (game.pause and game.fail) then
       game.pause = not game.pause 
    elseif game.pause and key == 'e' then
       love.event.quit()
    elseif (game.pause or game.fail) and key == 'r' then
        game:restart()
    elseif key == 'f' then
        Fullscreen = not Fullscreen
        love.window.setMode(800, 600, {resizable = true, fullscreen = Fullscreen, minwidth = 800, minheight = 600})
    elseif key == 'w' and snake.lastMove ~= 'down' then
        snake.moveDir = 'up'
        snake.lastMove = 'up'
    elseif key == 's' and snake.lastMove ~= 'up' then
        snake.moveDir = 'down'
        snake.lastMove = 'down'
    elseif key == 'a' and snake.lastMove ~= 'right' then
        snake.moveDir = 'left'
        snake.lastMove = 'left'
    elseif key == 'd' and snake.lastMove ~= 'left' then
        snake.moveDir = 'right'
        snake.lastMove = 'right'
    end
end

function love.focus(f)
    if not f then
        game.pause = true 
    end
end
