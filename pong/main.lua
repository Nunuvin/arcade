--[[ 
  Pong written with LOVE
  Avaliable under gplv3
  written by Vlad C.
]]




Ball = require "ball"
Paddle = require "paddle"

--[[
  Contains data related to global state of the game
]]
GameState = {
    --screen
    pause = false,
    windowWidth = 800,
    windowHeight = 600,
    fullscreen = false,
    fontSize = 48,
    paddleAlternator = 0,
    --time
    accumulator = 0,

    --game obj
    P1 = {}, 
    P2 = {}, 
    Ball = {}, 
}

    
    
    --[[
        resets the game
    ]]
    function GameState:reset(player1score, player2score, dx)
        if dx == nil then
            dx = 6
        end
        accumulator = 0
        self.P1 = Paddle:Create(50, GameState.windowHeight/2 - GameState.P1.height/2, "w", "s")
        self.P1.score = player1score
        self.P2 = Paddle:Create(GameState.windowWidth - 50, GameState.windowHeight/2 - GameState.P2.height/2, "up", "down")
        self.P2.score = player2score
        self.Ball = Ball:Create(GameState.windowWidth/2,GameState.windowHeight/2)
        self.Ball.dx = dx
        self.paddleAlternator = nil
        print(dx, player2score, player1score)
    end

    function GameState:collision(paddle, ball)
        if paddle.x < ball.x + ball.radius/2 and paddle.x + paddle.width > ball.x - ball.radius/2
            and paddle.y < ball.y + ball.radius/2 and paddle.y + paddle.height > ball.y - ball.radius/2 then
            return true
        else
            return false
        end
    end

    function GameState:movePaddle(obj) --within y bounds
        if obj.y + obj.height + obj.speed < self.windowHeight and obj.y + obj.speed > 0 then
            obj.y = obj.y + obj.speed
        end
    end

    function GameState:moveBall(obj)
        if obj.y - obj.radius < 0 or obj.y + obj.radius > self.windowHeight then
            obj.dy = -obj.dy
            obj:move()
        end        
        if obj.x - obj.radius < 0 then
            self.P2.score = self.P2.score + 1
            local dx = -6
            self:reset(self.P1.score, self.P2.score, dx)
        elseif obj.x+obj.radius > self.windowWidth then
            self.P1.score = self.P1.score + 1
            local dx = 6
            self:reset(self.P1.score, self.P2.score, dx)
        end


        obj:move()
    end

    --[[
        given a line and offset positions the line yOffset lines away from center
        however, x is centered correctly
    ]]
    function GameState:renderLineRelativeToCenter(line, yOffset)
        xOffset = #line/2 * 4
        love.graphics.print({{0,0.5,.5,1}, line}, (GameState.windowWidth)/2 - xOffset, GameState.windowHeight/2 - yOffset)
    end

    --[[
        expects a table with lines of text to be displayed
        centered true is to center the textbox in the center of the screen
    ]]
    function GameState:renderText(text)
        i = math.floor(#text/2)
        for j=1,#text,1 do
            GameState:renderLineRelativeToCenter(text[j], math.floor((i-j) * GameState.fontSize/2))
        end
    end

    --[[
        bottom layer
        render the paddle, ball, score
    ]]
    function GameState:renderGame()
        GameState.P1:render()
        GameState.P2:render()
        GameState.Ball:render()
    end

    function GameState:renderUI()
        love.graphics.print({{1,0,0,1}, GameState.P1.score, {0,0,1,1}, GameState.P2.score}, GameState.windowWidth/2-20, 25)
    end
    
    --[[
        top layer
        render the pause menu
    ]]
    function GameState:renderMenu()
        text = {
            "Pin pong",
            "PAUSED",
            "press esc to unpause",
            "press q to quit",
            "r to reset score and start the game over",
            "press F12 to toggle fullscreen"
        }
        GameState:renderText(text)
    end

    function GameState:fullscreenAdjustPlayers(widthChangeCoefficient, heightChangeCoefficient)
        self.P1.y = self.P1.y * heightChangeCoefficient
        self.P1.x = self.P1.x * widthChangeCoefficient
        self.P1.height = self.P1.height * heightChangeCoefficient
        self.P1.width = self.P1.width * widthChangeCoefficient


        self.P2.y =  self.P2.y * heightChangeCoefficient
        self.P2.x = self.P2.x * widthChangeCoefficient
        self.P2.height = self.P2.height * heightChangeCoefficient
        self.P2.width = self.P2.width * widthChangeCoefficient
    end

    function GameState:fullscreenAdjustBall( widthChangeCoefficient, heightChangeCoefficient )
        self.Ball.y = self.Ball.y * heightChangeCoefficient 
        self.Ball.x = self.Ball.x * widthChangeCoefficient

        --since change in x and y is not identical smallest is applied to radius given that the radius will not become less then default
        if heightChangeCoefficient < widthChangeCoefficient and self.Ball.radius * heightChangeCoefficient > self.Ball.defaultRadius then
            self.Ball.radius = self.Ball.radius * heightChangeCoefficient
        elseif self.Ball.radius * widthChangeCoefficient > self.Ball.defaultRadius then
            self.Ball.radius = self.Ball.radius * widthChangeCoefficient
        end
    end

    function GameState:fullscreenAdjust()
                local oldwindowDim = {
        width = GameState.windowWidth,
        height = GameState.windowHeight
        }

        self.fullscreen = not self.fullscreen
        love.window.setFullscreen(self.fullscreen)
        self.windowWidth = love.graphics.getWidth()
        self.windowHeight = love.graphics.getHeight()

        local heightChangeCoefficient = self.windowHeight / oldwindowDim.height
        local widthChangeCoefficient = self.windowWidth / oldwindowDim.width
        
        self:fullscreenAdjustPlayers(widthChangeCoefficient, heightChangeCoefficient)

        self:fullscreenAdjustBall(widthChangeCoefficient, heightChangeCoefficient)
        
    end

--[[
  Sets up the window and initialize objects
]]
function love.load()
    love.window.setTitle("Pong")
    love.window.setMode(GameState.windowWidth, GameState.windowHeight, {fullscreen = false})
    love.graphics.newFont(GameState.fontSize)

    GameState.P1 = Paddle:Create(50, GameState.windowHeight/2 - 50, "w", "s")
    GameState.P2 = Paddle:Create(GameState.windowWidth - 50, GameState.windowHeight/2 - 50, "up", "down")
    GameState.Ball = Ball:Create(GameState.windowWidth/2,GameState.windowHeight/2)
    GameState.Ball:randomStart()
    GameState.maxYCollisionDistance = GameState.P1.height/2
end


--[[
  game logic
  ]]
function love.update(dt)
    if not GameState.pause then
       tick = 1/100
       GameState.accumulator = GameState.accumulator + dt 
        if GameState.accumulator >= tick then
            GameState.accumulator = 0
            --update the game
            GameState.P1:updateSpeed()
            GameState.P2:updateSpeed()
            GameState:movePaddle(GameState.P1)
            GameState:movePaddle(GameState.P2)
            GameState:moveBall(GameState.Ball)
        end
    end

    --paddleAlternator prevents ball from being stuck inside of the paddle
    if GameState:collision(GameState.P1, GameState.Ball) and GameState.paddleAlternator ~= 1 then
        if GameState.P1.y - GameState.Ball.y > 0 then
            GameState.Ball.dy = (GameState.P1.y + GameState.P1.height/2 - GameState.Ball.y)/GameState.maxYCollisionDistance * 5
        elseif GameState.P1.y - GameState.Ball.y < 0 then
            GameState.Ball.dy = (GameState.P1.y + GameState.P1.height/2  - GameState.Ball.y)/GameState.maxYCollisionDistance * 5
        else
            GameState.Ball.dy = 0
        end
        GameState.paddleAlternator = 1
        GameState.Ball.dx = -GameState.Ball.dx

    elseif GameState:collision(GameState.P2, GameState.Ball) and GameState.paddleAlternator ~= -1 then 
        if GameState.P2.y - GameState.Ball.y > 0 then
            GameState.Ball.dy = (GameState.P2.y + GameState.P1.height/2  - GameState.Ball.y)/GameState.maxYCollisionDistance * 5
        elseif GameState.P2.y - GameState.Ball.y < 0 then
            GameState.Ball.dy = (GameState.P2.y + GameState.P1.height/2 - GameState.Ball.y)/GameState.maxYCollisionDistance * 5
        else
            GameState.Ball.dy = 0
        end
        GameState.paddleAlternator = -1
        GameState.Ball.dx = -GameState.Ball.dx
    end
end


--[[
  Identify keypresses and process them
]]
function love.keypressed(key)
    if key == "escape" then
        GameState.pause = not GameState.pause
    elseif GameState.pause and (key == 'q' or key == 'Q') then
        love.event.quit()
    elseif GameState.pause and key == 'r' then
        GameState:reset(0,0)
    elseif key == "f12" and GameState.pause then --change resolution to fullscreen

        GameState:fullscreenAdjust()

    end

    if key == GameState.P1.moveUpKey then 
        GameState.P1:speedChange(1)
    end

    if key == GameState.P1.moveDownKey then
        GameState.P1:speedChange(-1)
    end

    if key == GameState.P2.moveUpKey then
        GameState.P2:speedChange(1)
    end

    if key == GameState.P2.moveDownKey then
        GameState.P2:speedChange(-1)
    end
end

function love.keyreleased(key)
    if key == GameState.P1.moveUpKey then 
        GameState.P1:speedChange(0)
        GameState.P1.applyFriction = true
    end

    if key == GameState.P1.moveDownKey then
        GameState.P1:speedChange(0)
        GameState.P1.applyFriction = true
    end

    if key == GameState.P2.moveUpKey then
        GameState.P2:speedChange(0)
        GameState.P2.applyFriction = true
    end

    if key == GameState.P2.moveDownKey then
        GameState.P2:speedChange(0)
        GameState.P2.applyFriction = true
    end   
end

--[[
  draw the game screen
]]
function love.draw()
    GameState:renderGame()
    GameState:renderUI()
    if GameState.pause then
        GameState:renderMenu()
    end

end


--[[
  if tabbed out pause the game
]]
function love.focus(f)
    if not f then
        GameState.pause = true
    end
end