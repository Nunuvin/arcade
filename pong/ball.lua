--[[ 
  Pong written with LOVE
  Avaliable under gplv3
  written by Vlad C.
]]


--[[
  ball class
]]
local ball = {
    defaultRadius = 10,
    radius = 10,
    x = 0,
    y = 0,
    speed = 6,
    dx = 6,
    dy = 0,

}
  --[[
      Creates new instance of the ball
  ]]
  function ball:Create(x, y, dx)
     if dx == nil then
      dx = 6
     end
     b={}
     for key, value in pairs(self) do
         b[key] = value

      end
      b.x = x
      b.y = y
      b.dx = dx
      return b
  end

  function ball:render()
    love.graphics.circle("fill", self.x, self.y, self.radius)
  end

  function ball:move()
      self.x = self.x + self.dx
      self.y = self.y + self.dy
  end

  function ball:randomStart()
    ballDir = math.random()
    if ballDir < .5 then
      self.dx = -self.speed
    else
      self.dx = self.speed
    end
  end

return ball