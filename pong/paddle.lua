--[[ 
  Pong written with LOVE
  Avaliable under gplv3
  written by Vlad C.
]]


--[[
  paddle class
]]
local paddle = {
    width = 15,
    height = 120,
    x = 0,
    y = 0,
    speedDir = 1,
    frictionFactor = 2,
    applyFriction = false,
    speed = 0,
    score = 0,
    moveSpeed = 8,
    moveUpKey = '', --player dep
    moveDownKey = '', --player dep
    }

    function paddle:speedChange(ddir)
        self.applyFriction = false
        if ddir == 1 and self.speed <= self.moveSpeed then
            self.speed = -self.moveSpeed
        elseif ddir == -1 and self.speed >= -self.moveSpeed then
            self.speed = self.moveSpeed
        end
        self.dir = ddir
    end

    function paddle:updateSpeed()
        if math.floor(self.speed) == 0 then
            self.speed = 0
        elseif self.speed > 0  and self.applyFriction then
            self.speed = self.speed - self.frictionFactor
        elseif self.speed < 0 and self.applyFriction then
            self.speed = self.speed + self.frictionFactor
        end
    end

    function paddle:render()
        love.graphics.rectangle( "fill", self.x, self.y, self.width, self.height )
    end
    --[[
        creates a new deep copy of paddle with custom settings
    ]]
    function paddle:Create(x, y, up, down)
        p = {}
        --deep copy
        for orig_key, orig_value in pairs(paddle) do
            p[orig_key] = orig_value
        end
        
        p.x = x
        p.y = y
        p.moveUpKey = up
        p.moveDownKey = down
        return p        
    end
    
return paddle